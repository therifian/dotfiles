#############################################################################################
#                                                                                           #
#                                                                                           #
#       ███████╗███████╗██╗  ██╗       ██████╗ ██████╗ ███╗   ██╗███████╗██╗ ██████╗        #
#       ╚══███╔╝██╔════╝██║  ██║      ██╔════╝██╔═══██╗████╗  ██║██╔════╝██║██╔════╝        #
#         ███╔╝ ███████╗███████║█████╗██║     ██║   ██║██╔██╗ ██║█████╗  ██║██║  ███╗       #
#        ███╔╝  ╚════██║██╔══██║╚════╝██║     ██║   ██║██║╚██╗██║██╔══╝  ██║██║   ██║       #
#       ███████╗███████║██║  ██║      ╚██████╗╚██████╔╝██║ ╚████║██║     ██║╚██████╔╝       #
#       ╚══════╝╚══════╝╚═╝  ╚═╝       ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝     ╚═╝ ╚═════╝        #
#                                                                                           #
#                                                                                           #
#                                                                                           #
#############################################################################################
                                                                             
# Aliases
alias ll="ls -lh --color"
alias la="ls -lAh --color"
alias rm="rm -rf"

alias pn="pnpm"
alias ag="npx @angular/cli ng new --package-manager pnpm --skip-git --skip-install"
alias ng="npx ng"
alias rn="yarn create expo-app --template typescript"
alias rct="pnpm create react-app $1 --template typescript"
alias nxt="pnpm create next-app"
alias vue="pnpm create vue@3"
alias adonis="pnpm create adonis-ts-app"

alias gi="git init"
alias ga="git add"
alias gs="git status"
alias gf="git fetch"
alias gc="git commit"
alias gl="git log --oneline"
alias gps="git push"
alias gpl="git pull"
alias gca="git commit --amend"

# Exports
export PATH="$HOME/flutter/bin:$PATH"
export PATH="/usr/local/opt/node@18/bin:$PATH"
export PATH="/usr/local/opt/openjdk@17/bin:$PATH"
source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

[[ ! -f ~/.zsh/.p10k.zsh ]] || source ~/.zsh/.p10k.zsh
