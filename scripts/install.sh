#!/bin/bash

HELP="Usage: $0 [option] [argument]

Options:
    -a      full system with i3 gui but without tools 
    
    -b      basic system without gui or tools
    
    -c      basic system with cli

    -g      gui using i3 wm

    -m      macos install 

    -p      utils to install d: dev tools 
                             o: devops tools
                             h: hacking tools
"

function install_basic_system {
    printf "\e[1;36mStarting to format\e[0m\n"
    printf "\e[0;33mChoose a disk to partition above (caution it will be deleted)\e[0m\n\n" 
    lsblk
    printf "\nMake your chose: "
    read disk
    sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/$disk
    o       # Clear the partition table
    n       # Create new partition
    p       # Primary partition
    1       # partition number
            # Start at the default value
    +4G     # Swap partition space 4G
    t       # Change partition type
    1       # Partition number
    82      # Partition type 82 Swap type
    n       # Create new partition
    p       # Primary partition
    2       # partition number
            # Start at the default value
            # Use the avelaible
    t       # Change partition type
    1       # Partition number
    8e      # Partition type 8e LVM
    w       # Write changes
    q       # Quit 
EOF
    mkfs.ext4 /dev/sda2
    mkswap /dev/sda1
    mount /dev/sda2 /mnt
    swapon /dev/sda1
    pacman-key --init
    pacstrap -K /mnt base git linux linux-headers vim
    genfstab -U /mnt >> /mnt/etc/fstab
    printf "\e[1;32mFinished!\nYou are now in /mnt\nDownload dotfiles again and continue!\e[0m\n"
    arch-chroot /mnt/
}

function linkin_zsh {
    printf "Current home path (/home/username): "
    read userdir
    rm -rf $userdir/.bash* 2> /dev/null
    rm -rf $userdir/.config 2> /dev/null
    rm -rf $userdir/.gitconfig 2> /dev/null
    rm -rf $userdir/.zshrc 2> /dev/null
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $userdir/.zsh/powerlevel10k
    git clone https://github.com/zsh-users/zsh-autosuggestions $userdir/.zsh/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $userdir/.zsh/zsh-syntax-highlighting
    ln -s $userdir/.dotfiles/.config $userdir/.config
    ln -s $userdir/.dotfiles/.gitconfig $userdir/.gitconfig
    ln -s $userdir/.dotfiles/.p10k.zsh $userdir/.zsh/.p10k.zsh
    ln -s $userdir/.dotfiles/.zshrc $userdir/.zshrc
}

function install_cli_system {
    echo -n "Enter hostname: ";
    read hostname;
    echo -n "Enter username: ";
    read username;
    echo -n "Enter user password: ";
    read -s password;
    echo
    echo -n "Enter root password: ";
    read -s rootpassword;
    echo

    ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
    hwclock --systohc
    sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
    locale-gen
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf
    echo "KEYMAP=en_US-UTF-8" >> /etc/vconsole.conf
    echo "$hostname" >> /etc/hostname
    echo "127.0.0.1 localhost" >> /etc/hosts
    echo "::1       localhost" >> /etc/hosts
    echo "127.0.1.1 $hostname.localdomain $hostname" >> /etc/hosts
    echo root:$rootpassword | chpasswd
    pacman -S --noconfirm base-devel git grub linux-headers networkmanager network-manager-applet reflector vim wget xdg-user-dirs zsh
    grub-install --target=i386-pc /dev/sda
    grub-mkconfig -o /boot/grub/grub.cfg
    systemctl enable NetworkManager
    systemctl enable reflector.timer
    useradd -m $username
    echo $username:$password | chpasswd
    echo "$username ALL=(ALL) ALL" >> /etc/sudoers.d/$username
    printf "\e[1;36m[ENABLE ZSH]\e[0m\n"
    rm -rf ˜/.bash*
    touch /home/$username/.zshrc
    chsh -s /usr/bin/zsh $username
    printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m\n"
    exit
}

function install_gui_system {
    printf "\e[1;36m[SYSTEM UPDATE]\e[0m\n"
    pacman -Syu --noconfirm 
    printf "\e[1;36m[INSTALLING I3WM]\e[0m\n"
    pacman -S --noconfirm alsa-utils btop chromium dunst fd feh firefox i3-gaps kitty neovim picom polybar pulseaudio ranger ripgrep rofi sddm tmux ttf-dejavu unzip w3m xorg-xrandr zathura zathura-pdf-mupdf
    systemctl enable sddm
    printf "\e[1;36m[INSTALLING PARU]\e[0m\n"
    sudo -u "$SUDO_USER" -- sh -c "
    git clone https://aur.archlinux.org/paru.git /tmp/paru 
    cd /tmp/paru
    makepkg -si 
    paru --noconfirm
    paru -S --noconfirm brave-bin cava cmus nvim-packer-git ttf-jetbrains-mono-nerd > /dev/null"
    printf "\e[1;36m[MAKE SYMLINKS]\e[0m\n"
    git clone --depth 1 https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim
    git clone https://github.com/nautilor/nord-sddm.git /tmp/nord-sddm
    cp -R /tmp/nord-sddm/Nord /usr/share/sddm/themes/
    sed -i 's/Current=/Current=Nord/g' /usr/lib/sddm/sddm.conf.d/default.conf
    echo "xrandr --output Virtual-1 --mode 1920x1080" >> /usr/share/sddm/scripts/Xsetup
    linkin_zsh 
}

function install_macos {
    printf "\e[1;36m[SYSTEM DEV TOOLS]\e[0m\n"
    xcode-select --install
    printf "\e[1;36m[HOMEBREW INSTALL]\e[0m\n"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" 
    brew update
    printf "\e[1;36m[PACKAGES INSTALL]\e[0m\n"
    brew bundle --file ~/.dotfiles/packages/Brewfile 
    printf "\e[1;36m[CONFIGURATION]\e[0m\n"    
    linkin_zsh
}

function install_packages {
    local package=$1 
    local filename 
    printf "Path to file: (./.dotfiles/packages): "
    read path
    case $package in
        d)
            filename="devtools.txt"
            ;;
        h)
            filename="hacking.txt"
            ;;
        o)
            filename="devops.txt"
            ;;
        *)  
            printf "\e[1;31mERROR: Choose valid package (d h o)\e[0m\n"
            echo "$HELP"
            exit 1
            ;;
        esac   
    printf "\e[1;36m[SYSTEM UPDATE]\e[0m\n"
    pacman -Syu --noconfirm > /dev/null  
    while read line; 
    do 
        printf "\e[1;36m[INSTALLING: $line]\e[0m\n"
        pacman -S --noconfirm $line > /dev/null 
    done < "${path}/${filename}"
} 


while getopts abcgmp: name
do
    case $name in
        a)  
            install_cli_system
            install_gui_system
            ;;
        b)  
            install_basic_system
            ;;
        c)  
            install_cli_system
            ;;
        g)  
            install_gui_system
            ;;
        m)
            install_macos
            ;;
        p) 
            install_packages ${OPTARG}
            ;;
        *)  
            echo "$HELP" 
            exit 1
            ;;
    esac
done
