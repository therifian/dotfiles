vim.g.mapleader = " "

vim.keymap.set("n", "<leader>b", ":NvimTreeToggle<CR>")
vim.keymap.set("n", "<leader>t", ":ToggleTerm<CR>")

vim.keymap.set("n", "<C-S>", ":w<CR>")

vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)

vim.keymap.set("n", "<leader>gs", vim.cmd.Git)
vim.keymap.set("n", "<leader>gl", vim.cmd.LazyGit)

vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")
vim.keymap.set("n", "<C-l>", "<C-w>l")

vim.keymap.set("n", "<S-Up>", ":resize +2<CR>")
vim.keymap.set("n", "<S-Down>", ":resize -2<CR>")
vim.keymap.set("n", "<S-Left>", ":vertical resize -2<CR>")
vim.keymap.set("n", "<S-Right>", ":vertical resize +2<CR>")

vim.keymap.set("n", "<leader>ww", "<C-W>p")
vim.keymap.set("n", "<leader>wd", "<C-W>c")
vim.keymap.set("n", "<leader>w-", "<C-W>s")
vim.keymap.set("n", "<leader>w|", "<C-W>v")
vim.keymap.set("n", "<leader>-", "<C-W>s")
vim.keymap.set("n", "<leader>|", "<C-W>v")

vim.keymap.set("n", "<leader>j", ":bprevious<CR>")
vim.keymap.set("n", "<leader>k", ":bNext<CR>")
vim.keymap.set("n", "<leader>q", ":bdelete<CR>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")
