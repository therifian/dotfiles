local lsp = require('lsp-zero')

lsp.preset("recommended")

lsp.ensure_installed({
    'angularls',
    'asm_lsp',
    'awk_ls',
    'bashls',
    'clangd',
    'csharp_ls',
    'cssls',
    'docker_compose_language_service',
    'dockerls',
    'eslint',
    'golangci_lint_ls',
    'gopls',
    'html',
    'intelephense',
    'jdtls',
    'lua_ls',
    'pylyzer',
    'rust_analyzer',
    'sqlls',
    'tailwindcss',
    'tsserver',
    'vuels',
    'yamlls'
})

lsp.setup_servers({'dartls', force = true})

lsp.on_attach(function(client, bufnr)
  lsp.default_keymaps({buffer = bufnr})
end)

lsp.setup()
