require('nvim-treesitter.configs').setup {
    ensure_installed = {
        "bash",
        "c",
        "c_sharp",
        "cpp",
        "css",
        "dart",
        "dockerfile",
        "go",
        "java",
        "javascript",
        "lua",
        "php",
        "python",
        "rust",
        "typescript"
    },
    sync_install = false,
    auto_install = true,

    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
}
